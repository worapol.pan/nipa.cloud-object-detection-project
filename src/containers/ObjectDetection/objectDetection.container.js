import React, { useCallback, useRef, useState } from 'react'
import Layout from "components/Layout/layout.component"
import Webcam from "components/Webcam"
import { Button } from "@material-ui/core"
import { WebcamLayout } from "components/Webcam/webcam.styled"
import { PreviewScrollWrapper, useButtonStyle } from "containers/ObjectDetection/objectDetection.styled"
import enhance from './objectDetection.enhance'
import PreviewObjectDetection from "components/PreviewObjectDetection/previewObjectDetection.component"

const ObjectDetection = (props) => {
	const { queryObjectDetection, createNewObjectDetection } = props;
	const [queryDetection, setQueryDetection] = useState([]);
	const webcamRef = useRef()
	const classes = useButtonStyle()

	const capture = useCallback(() => {
		let screenshot = webcamRef.current.getScreenshot()
		queryObjectDetection(screenshot).then((result) => {
			setQueryDetection((prevData) => [
				...prevData,
				createNewObjectDetection(screenshot, result)
			])
		})
	}, [webcamRef, queryObjectDetection, createNewObjectDetection])

	const clear = useCallback(() => {
		setQueryDetection([])
	}, [])

	return (
		<Layout>
			<WebcamLayout>
				<Webcam ref={webcamRef}/>
				<Button
					variant="contained"
					color='primary'
					className={classes}
					onClick={capture}
				>
					Capture image
				</Button>
				<Button
					variant="contained"
					color='secondary'
					className={classes}
					onClick={clear}
				>
					Clear image
				</Button>
			</WebcamLayout>
			<PreviewScrollWrapper>
				<PreviewObjectDetection data={queryDetection} />
			</PreviewScrollWrapper>
		</Layout>
	)
}

export default enhance(ObjectDetection)
