import { compose, withHandlers } from 'recompose'
import axios from 'axios'
import { base64Prefix, API_KEY } from "containers/ObjectDetection/objectDetection.const"

export default compose(
	withHandlers({
		queryObjectDetection: () => (base64) => {
			if (typeof base64 !== 'string') throw new Error(`Base65 expected string type but actually is ${typeof base64}.`)
			if (!base64.startsWith(base64Prefix)) throw new Error('input isn\'t base64 format')

			const url = 'https://nvision.nipa.cloud/api/v1/object-detection'
			const payload = {
				raw_data: base64.replace(base64Prefix, '')
			}
			return axios.post(url, payload, {
				headers: {
					Authorization: `ApiKey ${API_KEY}`
				}
			}).then((result) => {
				return result.data
			})
		},
		createNewObjectDetection: () => (screenshot, result) => {
			if (!result) throw new Error('need result parameter for valid return')
			return {
				id: btoa(new Date().valueOf().toString()),
				base64: screenshot || '',
				result: result.detected_objects
			}
		}
	})
)
