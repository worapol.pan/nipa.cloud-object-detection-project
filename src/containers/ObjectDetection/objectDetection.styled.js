import React from 'react'
import { makeStyles } from "@material-ui/core"
import styled from 'styled-components'

export const useButtonStyle = makeStyles(() => ({
}))

export const PreviewScrollWrapper = styled.div`
	overflow: auto;
	max-height: 500px;
`
