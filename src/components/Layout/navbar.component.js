import Toolbar from "@material-ui/core/Toolbar"
import AppBar from "@material-ui/core/AppBar"
import React from "react"
import BrandIcon from "components/BrandIcon"
import { makeStyles, Typography } from "@material-ui/core"

const useAppbarStyle = makeStyles(() => ({
	colorPrimary: {
		backgroundColor: 'white'
	},
	root: {
		boxShadow: 'none',
		borderBottom: '2px solid #f5f5f5'
	}
}))

const useTitleStyle = makeStyles((theme) => ({
	root: {
		color: '#2f3448',
		marginLeft: theme.spacing(2)
	}
}))

const Navbar = () => {
	const appbarClasses = useAppbarStyle()
	const titleClasses = useTitleStyle()
	return (
		<AppBar classes={appbarClasses} position='static'>
			<Toolbar>
				<BrandIcon />
				<Typography classes={titleClasses} color='primary'>Object detection system</Typography>
			</Toolbar>
		</AppBar>
	)
}

export default Navbar
