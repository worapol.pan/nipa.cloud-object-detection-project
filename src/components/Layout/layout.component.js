import React from 'react'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Navbar from "components/Layout/navbar.component"


const Layout = ({children}) => {
	return (
		<div>
			<Navbar />
			{children}
		</div>
	)
}

export default Layout
