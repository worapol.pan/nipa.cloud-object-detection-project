import React, { forwardRef } from 'react'
import ReactWebcam from "react-webcam"
import styled from "styled-components"

const videoConstraints = {
	width: 1280*.5,
	height: 720*.5,
	facingMode: "user"
};

const Webcam = forwardRef((props, ref) => {
	return (
		<ReactWebcam
			ref={ref}
			audio={false} {...props}
			screenshotFormat="image/jpeg"
			videoConstraints={videoConstraints}
		/>
	)
})

const WebcamStyled = styled(Webcam)`
	margin: auto;
`

export default WebcamStyled
