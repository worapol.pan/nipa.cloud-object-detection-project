import React, { useCallback, useEffect, useRef } from 'react'

const ImagePreview = (props) => {
	const { data } = props
	const canvasRef = useRef()

	const drawRect = useCallback((context) => {
		if (data.result) {
			data.result.forEach(({ bounding_box, name }) => {
				const {left, top, bottom, right} = bounding_box
				context.beginPath()
				context.lineWidth = "3";
				context.strokeStyle = "red";
				context.rect(left, top, right-left, bottom-top)
				context.stroke();

				// draw text name
				const fontSize = 20
				context.font = `${fontSize}px Arial`
				context.fillStyle = 'red'
				context.fillText(name, left+10, top+fontSize);
			})
		}
	}, [data])

	useEffect(() => {
		const canvas = canvasRef.current
		const context = canvas.getContext('2d')

		const image = new Image()
		image.src = data.base64
		image.onload = function() {
			// setup canvas dimension
			context.canvas.width  = image.width;
			context.canvas.height = image.height;

			// draw image with base64
			context.drawImage(image,0,0);
			drawRect(context)
		}
		// const width = 1280*.5,height = 720*.5
		// ref.current.width = width
		// ref.current.height = height
	}, [canvasRef])
	return (
		<canvas ref={canvasRef} />
	)
}

const PreviewObjectDetection = (props) => {
	const { data = [] } = props

	return (
		data.map((dataObject) => {
			return (
				<ImagePreview data={dataObject} key={dataObject.id} />
			)
		})
	)
}

export default PreviewObjectDetection

