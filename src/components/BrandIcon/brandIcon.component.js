import React from 'react'
import styled from 'styled-components'
import NipaIcon from 'assets/icon/brand.png'

const BrandIcon = styled.img.attrs({
	src: NipaIcon
})`
	width: 40px;
`

export default BrandIcon
